import { Theme } from "@material-ui/core";
import React, { Component } from "react";

import { withStyles, WithStyles } from "client/ui";
import { DesignComponent } from "./DesignComponent";
import { UserInfoComponent } from "./UserInfoComponent";

const styles = (theme: Theme) => ({
    container: {
        display: "flex",
        flexDirection: "column",
    },
});

interface IMaintProps extends WithStyles<typeof styles> {}

@withStyles(styles)
export class MainComponent extends Component<IMaintProps> {
    public render() {
        return (
            <div className={this.props.classes.container}>
                <UserInfoComponent />
                <DesignComponent />
            </div>
        );
    }
}
