import { inject, observer } from "mobx-react";
import React, { Component, ReactNode } from "react";

import { DoorOption, FloorOption, IOption, Option, OptionTypes, SelectableOption } from "client/models";
import { CapsuleStore, UserInfoStore } from "client/stores";
import { ContentWrapperComponent, withStyles, WithStyles } from "client/ui";

import { AdditionalOptionsComponent } from "./AdditionalOptionsComponent";
import { BaseboardOptionComponent } from "./BaseboardOptionComponent";
import { CeilOptionComponent } from "./CeilOptionComponent";
import { DoorOptionComponent } from "./DoorOptionComponent";
import { FloorOprionComponent } from "./FloorOprionComponent";
import { PackageOptionsComponent } from "./PackageOptionsComponent";
import { PowerswitchesOptionComponent } from "./PowerswitchesOptionComponent";
import { SummaryComponent } from "./SummaryComponent";
import { WallOptionComponent } from "./WallOptionComponent";

const styles = () => ({
    option: {
        ["&:nth-child(even)"]: {
            backgroundColor: "#fbfafa",
        },
        padding: "55px 5% 35px",
    },
});

interface IOptionsProps extends WithStyles<typeof styles> {
    capsuleStore?: CapsuleStore;
    userInfoStore?: UserInfoStore;
}

@withStyles(styles)
@inject(CapsuleStore.key, UserInfoStore.key)
@observer
export class OptionsComponent extends Component<IOptionsProps> {
    public renderChildren() {
        const { capsuleStore } = this.props;

        return capsuleStore.selectedCapsule.options.map(this.renderOption);
    }

    public render() {
        return (
            <>
                {React.Children.map(this.renderChildren(), (child) => this.renderWrapper(child))}
                {/*Button here*/}
            </>
        );
    }

    private renderOption = (option: IOption) => {
        const { capsuleStore } = this.props;
        const type = capsuleStore.selectedCapsule.type;
        const selectedOption = capsuleStore.selectedCapsule.getSelectedOption(option.type);
        const onSelect = capsuleStore.selectedCapsule.toggleSelected.bind(capsuleStore.selectedCapsule, option.type);

        switch (option.type) {
            case OptionTypes.wall:
                return (
                    <WallOptionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as SelectableOption[]}
                    />
                );
            case OptionTypes.floor:
                return (
                    <FloorOprionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as FloorOption[]}
                    />
                );
            case OptionTypes.ceil:
                return (
                    <CeilOptionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as SelectableOption[]}
                    />
                );
            case OptionTypes.door:
                return (
                    <DoorOptionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as DoorOption[]}
                    />
                );
            case OptionTypes.baseboard:
                return (
                    <BaseboardOptionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as SelectableOption[]}
                    />
                );
            case OptionTypes.power:
                return (
                    <PowerswitchesOptionComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as SelectableOption[]}
                    />
                );
            case OptionTypes.summary:
                return (
                    <SummaryComponent
                        key={`${type}-${option.type}`}
                        capsule={capsuleStore.selectedCapsule}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as Option[]}
                    />
                );
            case OptionTypes.additional:
                return (
                    <AdditionalOptionsComponent
                        key={`${type}-${option.type}`}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as Option[]}
                    />
                );
            case OptionTypes.package:
                return (
                    <PackageOptionsComponent
                        key={`${type}-${option.type}`}
                        capsule={capsuleStore.selectedCapsule}
                        userInfoStore={this.props.userInfoStore}
                        selected={selectedOption.values}
                        onSelect={onSelect}
                        options={option.options as Option[]}
                    />
                );
            default:
                return null;
        }
    }

    private renderWrapper = (node: ReactNode, key?: string): React.ReactNode | null => {
        return (
            <div key={key} className={this.props.classes.option}>
                <ContentWrapperComponent>
                    {node}
                </ContentWrapperComponent>
            </div>
        );
    }
}
