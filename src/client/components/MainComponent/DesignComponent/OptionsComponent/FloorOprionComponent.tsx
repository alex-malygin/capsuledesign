import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { FloorOption, IFloorDetail, ImageShape } from "client/models";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    caption: {
        color: "#bbbbbb",
        fontSize: 15,
    },
    checked: {
        border: "1px solid",
    },
    circle: {
        height: 76,
        marginRight: 24,
        width: "auto",
    },
    horizontal: {
        alignItems: "center",
        display: "flex",
        flexDirection: "row",
        marginBottom: 19,
    },
    img: {
        flex: "1",
    },
    label: {
        color: "#161616",
        marginBottom: 20,
        textTransform: "uppercase",
    },
    oct: {
        height: 44,
        marginRight: 24,
        width: 156,
    },
    option: {
        ["&:last-child"]: {
            marginRight: 0,
        },
        alignItems: "flex-start",
        marginRight: 50,
        minWidth: "23%",
        width: "22%",
    },
    order: {
        marginBottom: 12,
        order: -1,
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    rect: {
        height: 48,
        marginRight: 24,
        width: 94,
    },
    row: {
        display: "flex",
        flexDirection: "row",
        width: "90%",
    },
    variant: {
        borderRadius: "50%",
        height: 40,
        width: 40,
    },
    variantRoot: {
        ["&:last-child"]: {
            marginRight: 0,
        },
        marginRight: 24,
        padding: 5,
    },
    variants: {
        flexDirection: "row",
        marginTop: 14,
    },
    vertical: {
        alignItems: "flex-start",
        display: "flex",
        flexDirection: "column",
    },
});

interface IFloorOptionProps extends WithStyles<typeof styles> {
    options: FloorOption[];
    selected: string[];
    onSelect: (val: string) => void;
}

@withStyles(styles)
export class FloorOprionComponent extends Component<IFloorOptionProps> {
    private horizontal: boolean;

    constructor(props: IFloorOptionProps) {
        super(props);

        this.horizontal = this.isHorizontal(props.options);
    }

    public render() {
        const { classes, options } = this.props;

        return (
            <div>
                <Typography variant="title">Напольные покрытия</Typography>
                <div className={classes.row}>
                    {options.map(this.renderOption)}
                </div>
            </div>
        );
    }

    public renderOption = (option: FloorOption) => {
        const { classes, selected } = this.props;
        return (
            <div key={option.label} className={classNames(classes.row, classes.option)}>
                <Checkbox
                    value={option.value}
                    className={classes.radioClass}
                    checked={selected.indexOf(option.value) >= 0}
                    classes={{ root: classes.radio }}
                    onChange={this.onOptionSelect}
                    icon={<RadioButtonIcon checked={false} />}
                    checkedIcon={<RadioButtonIcon checked={true} />}
                />
                <div>
                    <Typography className={classes.label}>{option.label}</Typography>
                    {option.details.map(this.renderDetail)}
                </div>
            </div>
        );
    }

    public renderDetail = (detail: IFloorDetail, index: number) => {
        const { classes } = this.props;
        const multipleSrc = typeof detail.img !== "string";

        const containerClassName = classNames({
            [classes.horizontal]: this.horizontal,
            [classes.vertical]: !this.horizontal,
        });

        const captionClassName = classNames(
            classes.caption,
            {
                [classes.order]: !this.horizontal,
            },
        );

        const img = multipleSrc ?
            this.renderImage(detail.img as string[], detail.shape) :
            this.renderImage(detail.img as string, detail.shape);

        return (
            <div key={`${detail.shape}-${index}`} className={containerClassName}>
                <div>
                    {img}
                </div>
                <Typography className={captionClassName} variant="caption">
                    {detail.description}
                </Typography>
            </div>
        );
    }

    private isHorizontal(opts: FloorOption[]) {
        return opts.reduce<boolean>((acc, option) => {
            return acc && option.details.reduce<boolean>((a, v) => a && !Array.isArray(v.img), true);
        }, true);
    }

    private renderImage(src: string, shape: ImageShape): React.ReactNode;
    private renderImage(src: string[], shape: ImageShape): React.ReactNode[];

    private renderImage(src: string | string [], shape: ImageShape): any {
        const { classes } = this.props;
        const imgClassName = classNames(
            classes.img,
            {
                [classes.circle]: shape === ImageShape.circle,
                [classes.rect]: shape === ImageShape.rect,
                [classes.oct]: shape === ImageShape.oct,
            },
        );

        if (Array.isArray(src)) {
            return src.map((img) => (
                <img key={img} src={img} className={imgClassName} />
            ));
        } else {
            return (
                <img key={src} src={src} className={imgClassName} />
            );
        }
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }
}
