import { MuiThemeProvider, Theme } from "@material-ui/core";
import { Provider } from "mobx-react";
import React, { Component } from "react";

import { createTheme, withStyles, WithStyles } from "client/ui";
import { MainComponent } from "./components";
import { createStores, IStoreMap } from "./stores";

const styles = (theme: Theme) => ({
    root: {
        // minWidth: 1600,
    },
});

interface IAppProps extends WithStyles<typeof styles> {}

@withStyles(styles)
export default class App extends Component<IAppProps> {
    private stores: IStoreMap;
    private theme: Theme;

    constructor(props: {}) {
        super(props);
        this.stores = createStores();
        this.theme = createTheme();
    }

    public render() {
        return (
            <Provider {...this.stores}>
                <MuiThemeProvider theme={this.theme}>
                    <MainComponent />
                </MuiThemeProvider>
            </Provider>
        );
    }
}
