import { Option } from "./Option";

export enum ImageShape {
    circle,
    rect,
    oct,
}

export interface IFloorDetail {
    description: string;
    img: string | string[];
    shape: ImageShape;
}

export class FloorOption extends Option {
    private _details: IFloorDetail[];

    constructor(label: string, details: IFloorDetail[]) {
        super(label);

        this._details = details;
    }

    public get details() {
        return this._details;
    }
}
